# CMS PO&DAS Higgs PAG exercise

This repository contains the code for the Higgs PAG exercise at the CMS Physics Objects & Data Analysis School held in Hamburg in October 2023.

*Authors*: Matteo Bonanomi, Philip Keicher, Daniel Savoiu

The aim of this exercise is to reconstruct the Standard Model Higgs boson using a selection targeting the four-lepton final state. The exercise
is implemented in the [columnflow](https://github.com/columnflow/columnflow) framework, which provides a fully orchestrated analysis setup
featuring fast columnar ("array-at-a-time") event processing, histogramming with and plotting, as well as tools for organizing analysis
metadata (datasets, binnings, colors, etc.) and workflow management.


### Quickstart

Log in to a NAF school node, then switch to one of the NAF CMS nodes:

```bash
ssh <school_username>@naf-school.desy.de
ssh naf-cms.desy.de
```

Create a directory on the shared DUST space to store analysis code and outputs (event arrays, histograms, plots, etc.)
and switch to it:
```bash
mkdir -p /nfs/dust/cms/group/cmsdas2023/<cern_username>
cd /nfs/dust/cms/group/cmsdas2023/<cern_username>
```

We recommend that you fork the analysis repository, so you have your own copy of the code. The following steps will
assume you have placed the fork under your personal CERN username on GitLab:
`https://gitlab.cern.ch/<cern_username>/cms-podas23-hig.git`.

Clone the forked repository. We will clone over HTTPS so we don't have to bother with SSH keys for now:
```bash
git clone --recursive https://gitlab.cern.ch/<username>/cms-podas23-hig.git
```

Finally, switch to the freshly cloned repository and run the setup script to install the software:
```bash
cd cms-podas23-hig
source setup.sh dev
```

The setup should now run for a few minutes. When it's finished, you should see a line of green text stating
that the analysis has been successfully set up.


### Resources

- [Columnflow task graph](https://github.com/columnflow/columnflow/wiki#default-task-graph)
- [NanoAOD (v9) content documentation](https://cms-nanoaod-integration.web.cern.ch/autoDoc/NanoAODv9/2017UL/doc_TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8_RunIISummer20UL17NanoAODv9-106X_mc2017_realistic_v9-v1.html)

Documentation of the *AwkwardArray* and *Hist* packages used by *columnflow*.
- [AwkwardArray documentation](https://awkward-array.org/doc/main/)
- [Hist package documentation](https://hist.readthedocs.io/en/latest/user-guide/quickstart.html)

Repositories:
- [columnflow](https://github.com/columnflow/columnflow)
- [law](https://github.com/riga/law)
- [order](https://github.com/riga/order)
- [luigi](https://github.com/spotify/luigi)
