#
# set some common variables
# (for sourcing inside derived scripts)
#

# name of the config
export my_config="run2_2017_nano_v9_limited"

# version tag
export my_version="v0"

# signal process & dataset:
#   - Higgs boson (gluon fusion) decaying to 4 leptons
export my_sig_process="h_ggf_4l"
export my_sig_dataset="h_ggf_4l_powheg"

# background process & dataset
#   - standard model ttbar (semileptonic decay channel)
export my_bkg_process="zz_llll"
export my_bkg_dataset="zz_llll_powheg"

export all_bkg_processes="zz_llll,ggf_4l"
export all_processes="${all_bkg_processes},h_ggf_4l"

# categories
export my_categories="incl"

# print or run commands depending on env var PRINT
_law=$(type -fp law)
law () {
    if [ -z $PRINT ]; then
        ${_law} "$@"
    else
        echo law "$@"
    fi
}

_cf_inspect=$(type -fp cf_inspect)
cf_inspect () {
    if [ -z $PRINT ]; then
        ${_cf_inspect} "$@"
    else
        echo cf_inspect "$@"
    fi
}
